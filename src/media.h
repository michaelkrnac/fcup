#include <boost/filesystem.hpp>
#include <openssl/md5.h>
#include <iomanip>
#include <sstream>
#include <boost/iostreams/device/mapped_file.hpp>

class Media
{	
	protected:
		boost::filesystem::path path;
		std::string md5;
		
	
	public:
		Media(boost::filesystem::path _path);
		virtual ~Media();
		boost::filesystem::path getPath();
		void calculateMD5();
		std::string getMD5();
};
