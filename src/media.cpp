#include "media.h"

Media::Media(boost::filesystem::path _path) : path (_path)
{
}

Media::~Media()
{
}

boost::filesystem::path Media::getPath()
{
	return path;
}

void Media::calculateMD5()
{
    unsigned char result[20];
    boost::iostreams::mapped_file_source src(path);
    MD5((unsigned char*)src.data(), src.size(), result);

    std::ostringstream sout;
    sout<<std::hex<<std::setfill('0');
    for(auto c: result) sout<<std::setw(2)<<(int)c;

    md5 = sout.str();
}

std::string Media::getMD5()
{
	return md5;
}
