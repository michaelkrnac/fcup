#include <gtkmm/application.h>
#include "appwindow.h"

int main(int argc, char *argv[])
{
  auto app = Gtk::Application::create(argc, argv, "org.fcup");

  AppWindow window;

  return app->run(window);
}
