#include "medialist.h"

MediaList::MediaList() : rootPath ("notdefined")
{
}

MediaList::~MediaList()
{
	list.clear();
}

void MediaList::setRootPath(Glib::ustring _root_path)
{
	rootPath = _root_path;
}

Glib::ustring MediaList::getRootPath()
{
	return rootPath;
}

void MediaList::loadMedia()
{
	
	path current_dir(rootPath);
	boost::regex pattern(".*.(jpg|JPG)");
	recursive_directory_iterator iter(current_dir), end;

	for (; iter != end;	++iter)
	{
		string name = iter->path().filename().string();
		if (regex_match(name, pattern))
		{
			Media media(iter->path());
			list.push_back(media);
		}
	}
}

void MediaList::printMediaList()
{
	for(auto & media : list)
	{
		media.calculateMD5();
		cout << media.getPath() << " --- " << media.getMD5() << endl;
	}
}
