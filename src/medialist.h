#include <gtkmm.h>
#include <iostream>
#include <vector>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include "media.h"

using namespace std;
using namespace boost::filesystem;

class MediaList
{
	protected:
		Glib::ustring rootPath;
		vector<Media> list;

	public:
		MediaList();
		virtual ~MediaList();
		void setRootPath(Glib::ustring _root_path);
		Glib::ustring getRootPath();
		virtual void loadMedia();
		virtual void printMediaList();
};
