#include "appwindow.h"

AppWindow::AppWindow()
{
	set_title("fcup");
	set_default_size(600, 400);
	
	m_headerbar.set_title("fcup");
	m_headerbar.set_subtitle("file clean up tool");
	m_headerbar.set_show_close_button(true);
	set_titlebar(m_headerbar);

	m_box.pack_start(m_dir, true, true, 0);
	m_dir.set_action(Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER);
	m_dir.signal_file_set().connect(sigc::mem_fun(*this, &AppWindow::onDirSelect));
	add(m_box);

	show_all_children();
}

AppWindow::~AppWindow()
{
}

void AppWindow::onDirSelect()
{	
	mediaList.setRootPath(m_dir.get_filename());
	std::cout << "Selected folder: " << mediaList.getRootPath() << std::endl;
	mediaList.loadMedia();
	mediaList.printMediaList();
}
