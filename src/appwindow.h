#ifndef APPWINDOW_H
#define APPWINDOW_H

#include <iostream>
#include <gtkmm/applicationwindow.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/box.h>
#include <gtkmm/filechooserbutton.h>
#include <gtkmm/enums.h>
#include "medialist.h"

class AppWindow : public Gtk::ApplicationWindow
{
	protected:
		MediaList mediaList;
		Gtk::HeaderBar m_headerbar;
		Gtk::Box m_box;
		Gtk::FileChooserButton m_dir;
		void onDirSelect();

	public:
		AppWindow();
		virtual ~AppWindow();
};

#endif //APPWINDOW_H
