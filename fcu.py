import gi
import os
import hashlib
import time
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GdkPixbuf
from collections import defaultdict

class File:
    BUFFER_SIZE = 8129
    absPath = ""
    relPath = ""
    name = ""
    date = ""

    def __init__(self,_root, _path, _fileName):
        self.absPath = os.path.join(_path,  _fileName)
        self.relPath = _path.replace(_root,"")
        self.name = _fileName
        t = time.gmtime(os.path.getmtime(self.absPath))
        self.date = time.strftime("%Y-%m-%d",t)

    def calculate_md5(self):
        m = hashlib.md5()
        f = open(self.absPath,'rb')
        while True:
            buf = f.read(self.BUFFER_SIZE)
            if not buf:
                break
            m.update(buf)
        self.md5Digest = m.hexdigest()
        f.close()


class MyWindow(Gtk.Window):
    mediaType = "IMAGE"

    def __init__(self):
        Gtk.Window.__init__(self, title="Hello World")
        self.set_default_size(800,400)
      
        self.headerBar = Gtk.HeaderBar()
        self.headerBar.set_show_close_button(True)
        self.set_titlebar(self.headerBar)
        
        self.folderChooser = Gtk.FileChooserButton()
        self.folderChooser.set_title("Select root folder")
        self.folderChooser.set_action(Gtk.FileChooserAction.SELECT_FOLDER)
        self.folderChooser.connect("file-set", self.on_folder_selected)
        self.headerBar.pack_start(self.folderChooser)
        
        self.imageButtonImage = Gtk.Image().new_from_icon_name("image-x-generic",Gtk.IconSize.BUTTON)
        self.imageButton = Gtk.ToggleButton()
        self.imageButton.set_image(self.imageButtonImage)
        self.imageButton.set_active(True)
        self.imageButton.connect("clicked", self.on_image_select)
        self.headerBar.pack_start(self.imageButton)

        self.videoButtonImage = Gtk.Image().new_from_icon_name("video-x-generic",Gtk.IconSize.BUTTON)
        self.videoButton = Gtk.ToggleButton()
        self.videoButton.set_image(self.videoButtonImage)
        self.videoButton.connect("clicked", self.on_video_select)
        self.headerBar.pack_start(self.videoButton)

        self.audioButtonImage = Gtk.Image().new_from_icon_name("audio-x-generic",Gtk.IconSize.BUTTON)
        self.audioButton = Gtk.ToggleButton()
        self.audioButton.set_image(self.audioButtonImage)
        self.audioButton.connect("clicked", self.on_audio_select)
        self.headerBar.pack_start(self.audioButton)

        self.settingButtonImage = Gtk.Image().new_from_icon_name("applications-system",Gtk.IconSize.BUTTON)
        self.settingButton = Gtk.Button()
        self.settingButton.set_image(self.settingButtonImage)
        self.headerBar.pack_end(self.settingButton)
      
     
        self.boxContainer = Gtk.Box().new(Gtk.Orientation.VERTICAL,spacing=6)
        self.add(self.boxContainer)

        self.ActionBarTop = Gtk.ActionBar()
        self.boxContainer.pack_start(self.ActionBarTop, False, True, 0)
    
        self.pathLabel = Gtk.Label.new("No root folder selected")
        self.ActionBarTop.pack_start(self.pathLabel)

        self.duplicateButtonImage = Gtk.Image().new_from_icon_name("edit-find",Gtk.IconSize.BUTTON)
        self.duplicateButton = Gtk.Button.new_with_label("Find duplicates")
        self.duplicateButton.set_image(self.duplicateButtonImage)
        self.duplicateButton.set_always_show_image(True)
        self.duplicateButton.set_sensitive(False)
        self.duplicateButton.connect("clicked", self.on_find_duplicate)
        self.ActionBarTop.pack_end(self.duplicateButton)

        self.scroll = Gtk.ScrolledWindow()

        self.treeStore = Gtk.TreeStore.new([str, str, str, GdkPixbuf.Pixbuf, bool, bool, str])
        self.folderTree = Gtk.TreeView.new_with_model(self.treeStore)

        renderText = Gtk.CellRendererText()
        renderPixbuf = Gtk.CellRendererPixbuf()
        renderRadio = Gtk.CellRendererToggle()
        renderRadio.set_radio(True)
        renderRadio.connect("toggled", self.on_radio_toggled)

        colPath = Gtk.TreeViewColumn('File Path', renderText, text=0)
        self.folderTree.append_column(colPath)

        colFile = Gtk.TreeViewColumn('File Name', renderText, text=1)
        self.folderTree.append_column(colFile)
        
        colDate = Gtk.TreeViewColumn('File Date', renderText, text=2)
        self.folderTree.append_column(colDate)
        
        colImage = Gtk.TreeViewColumn('Image', renderPixbuf, pixbuf=3)
        self.folderTree.append_column(colImage)
        
        colKeep = Gtk.TreeViewColumn('Keep', renderRadio, active=4)
        self.folderTree.append_column(colKeep)

        colDelete = Gtk.TreeViewColumn('Delete', renderRadio, active=5)
        self.folderTree.append_column(colDelete)
        
        colEmpty = Gtk.TreeViewColumn('', renderText, text=6)
        self.folderTree.append_column(colEmpty)
        
        self.scroll.add(self.folderTree)
        self.boxContainer.pack_start(self.scroll, True, True, 0)

        self.ActionBarBottom = Gtk.ActionBar()
        self.boxContainer.pack_start(self.ActionBarBottom, False, True, 0)
  
        self.foundLabel = Gtk.Label.new("No duplicates search yet")
        self.ActionBarBottom.pack_start(self.foundLabel)

        self.cleanupButtonImage = Gtk.Image().new_from_icon_name("user-trash",Gtk.IconSize.BUTTON)
        self.cleanupButton = Gtk.Button.new_with_label("Cleanup")
        self.cleanupButton.set_image(self.cleanupButtonImage)
        self.cleanupButton.set_always_show_image(True)
        self.cleanupButton.set_sensitive(False)
        self.cleanupButton.connect("clicked", self.on_cleanup)
        self.ActionBarBottom.pack_end(self.cleanupButton)
    
    def on_image_select(self, widget):
        self.mediaType = "IMAGE"
        self.videoButton.set_active(False)
        self.audioButton.set_active(False)
            
    def on_video_select(self, widget):
        self.mediaType = "VIDEO"
        self.imageButton.set_active(False)
        self.audioButton.set_active(False)
        
    def on_audio_select(self, widget):
        self.mediaType = "AUDIO"
        self.imageButton.set_active(False)
        self.videoButton.set_active(False)
        
    def on_radio_toggled(self, widget, path):
        self.treeStore[path][4] = not self.treeStore[path][4] 
        self.treeStore[path][5] = not self.treeStore[path][5] 

    def on_folder_selected(self, widget):
        self.rootFolder = self.folderChooser.get_filename() 
        self.pathLabel.set_text(self.rootFolder)
        self.duplicateButton.set_sensitive(True)

    def on_find_duplicate(self, widget):
        self.treeStore.clear()
        file_dict = defaultdict(list)
        i = 0
        for cdir, dirs, files in os.walk(self.rootFolder):
            for file in files:
                if file.endswith(".JPG"):
                    f = File(self.rootFolder, cdir, file)
                    f.calculate_md5()
                    file_dict[f.md5Digest].append(f)
                    i += 1
                    print(i)
        
        for md5, files in file_dict.items():
            if len(files) > 1:
                print(files[0].name)
                parent = self.treeStore.append(None,[md5,"", "", None, False, False, ""])
                for f in files:
                    pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(f.absPath,-1,100, True)
                    self.treeStore.append(parent,[f.relPath, f.name, f.date, pixbuf, True, False, ""])

        self.foundLabel.set_text("Found " + str(len(self.treeStore)) + " files with duplicates")
        self.cleanupButton.set_sensitive(True)

    def on_cleanup(self, widget):
        print("Cleanup")

win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
