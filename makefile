# change application name here (executable output name)
TARGET=fcup
 
# compiler
CC=g++
# debug
DEBUG=-g
# optimisation
OPT=-O
# warnings
WARN=-Wall
 
PTHREAD=-pthread
 
CCFLAGS=$(DEBUG) $(OPT) $(WARN) $(PTHREAD) -pipe
 
GTKMM=`pkg-config gtkmm-3.0 --cflags --libs`
BOOST= -lboost_system -lboost_filesystem -lboost_regex -lboost_iostreams -lcrypto

# linker
LD=g++
LDFLAGS=$(PTHREAD) -export-dynamic
 
OBJS=main.o appwindow.o medialist.o media.o
 
# target all
all: $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(GTKMM) $(BOOST) $(LDFLAGS)

# compile
%.o: src/%.cpp
	$(CC) -c $(CCFLAGS) $< $(GTKMM) $(BOOST)

# target clean
clean:
	rm -f *.o $(TARGET)
